package scala.com.hilabs.analytics

import com.hilabs.analytics.fpg.{ARules, GenericARule => Rule, PatternQuery, Query}
import org.apache.spark.sql.SparkSession
import org.scalatest.{FlatSpec, Matchers}
import com.hilabs.analytics.fpg.Query._

class ARulesTest extends FlatSpec with Matchers{

  lazy val spark: SparkSession = {
    val session =
      SparkSession
        .builder()
        .master("local[4]")
        .appName("spark sample project")
        .getOrCreate()
    session.sparkContext.setLogLevel("WARN")
    session
  }

  import spark.implicits._

  val associationRules =
    new ARules(
      spark,
      Query(PatternQuery(false, eql("a"))), //The antecedent should have atleast one a in consequent
      eql("c"), //The consequent should equal c
      4, //The max pattern/rule length. This implies, in antecedent, there could be atmost two more additional elements other than a
      minItemSupport = 1,
      minRuleSupport = 1)

  val chrLst = List("ab", "bcd", "acde", "ade", "abc", "abcd", "a", "abc", "abd", "bce")
  val chars: Seq[Array[String]] = chrLst.map(_.split("").map(_.head.toString))
  val transactions = spark.sparkContext.parallelize(chars)

  "Association rules with a given confidence" should "be as expected" in {
    val rules = associationRules.generateRules(transactions)
    rules.collect().toSet should equal(
      Set(
        Rule("c",List("e", "d", "a"),1,2,6,8),
        Rule("c",List("b", "a"),3,5,6,8),
        Rule("c",List("a"),4,8,6,8),
        Rule("c",List("e", "a"),1,2,6,8),
        Rule("c",List("d", "a"),2,4,6,8),
        Rule("c",List("d", "b", "a"),1,2,6,8)))
    }

  "The number of item queries in antecedentQuery" should "be one less than maxRuleLength" in {
    assertThrows[IllegalArgumentException] {
      new ARules(
        spark,
        Query(PatternQuery(false, eql("a")), PatternQuery(false, eql("a"), or(eql("b"), eql("b")), Query.not(eql("c")), Query.in(Set("d", "e")))),
        eql("c"),
        4,
        minItemSupport = 1,
        minRuleSupport = 1)
    }
  }
}
