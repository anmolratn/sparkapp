package scala.com.hilabs.analytics

import com.hilabs.analytics.FPG
import org.apache.spark.mllib.fpm.FPGrowth
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.scalatest.{FlatSpec, Matchers}

class FPGTest extends FlatSpec with Matchers {

  lazy val spark: SparkSession = {
    val session =
      SparkSession
        .builder()
        .master("local[4]")
        .appName("spark sample project")
        .getOrCreate()
    session.sparkContext.setLogLevel("WARN")
    session
  }

  val chrLst = List("ab", "bcd", "acde", "ade", "abc", "abcd", "a", "abc", "abd", "bce")
  val chars: Seq[Array[Char]] = chrLst.map(_.split("").map(_.head))
  val transactions = spark.sparkContext.parallelize(chars)
  val fpg = new FPG[Char](transactions, 0.2, 2)

  "Frequent Itemset and their counts" should "be as expected" in {
    println(s"transaction count=${transactions.count()}")
    val frequentItemSets: Array[FPGrowth.FreqItemset[Char]] = fpg.frequentItemsSets.collect()
    frequentItemSets.length should equal(19)
    val frequentItemSetCountsStr = frequentItemSets.map(itemset => s"${itemset.items.mkString(",")}:${itemset.freq}" )
    frequentItemSetCountsStr.toSet should equal(Set("e:3", "e,c:2", "e,d:2", "e,d,a:2", "e,a:2", "a:8",
      "b:7", "b,a:5", "d:5", "d,c:3", "d,c,b:2", "d,c,a:2", "d,b:3", "d,b,a:2", "d,a:4", "c:6", "c,b:5", "c,b,a:3", "c,a:4"))
  }

  "Association rules with a given confidence" should "be as expected" in {
    val rules = fpg.associationRules(0.8).collect()
    val ruleStrs =
      rules.map(rule =>
        s"${rule.antecedent.mkString(",")} -> ${rule.consequent.mkString(",")} : ${rule.confidence.formatted("%.2f")}")
    ruleStrs.toSet should equal(Set("d -> a : 0.80", "e,d -> a : 1.00", "e,a -> d : 1.00", "c -> b : 0.83"))
  }


}
