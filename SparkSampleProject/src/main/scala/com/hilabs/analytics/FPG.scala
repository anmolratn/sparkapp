package com.hilabs.analytics

import org.apache.spark.mllib.fpm.{AssociationRules, FPGrowth, FPGrowthModel}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

import scala.reflect.ClassTag

class FPG[Item: ClassTag](transactions: RDD[Array[Item]], minSupport: Double, numPartitions: Int) {

  lazy val model: FPGrowthModel[Item] =
    new FPGrowth()
      .setMinSupport(minSupport)
      .setNumPartitions(numPartitions)
      .run(transactions)

  lazy val frequentItemsSets: RDD[FPGrowth.FreqItemset[Item]] = model.freqItemsets

  def associationRules(confidence: Double): RDD[AssociationRules.Rule[Item]] =
    model.generateAssociationRules(confidence)

}


object FPG {
  def main(args: Array[String]): Unit = {
    //    val master = "yarn" //Replace this with "local" if you want to run this code in local mode and also remove provided tag for spark-mllib_2.11 in pom.xml.
    val master = "local"
    val spark: SparkSession = SparkSession.builder.master(master).appName(this.getClass.getName).getOrCreate()
    spark.sparkContext.setLogLevel("INFO") //change this to WARN if you don't want to spam your screen with all the log statements

    //Sample usage. Change this as necessary. E.g. read the data from parquet files with the paths given as commandline arguments
    val intLst = List(Array(1, 3), Array(1, 5), Array(1, 2, 3, 4), Array(1, 3, 5), Array(1, 2, 3), Array(1), Array(2, 3, 5))
    val transactions = spark.sparkContext.parallelize(intLst)
    val fpg = new FPG[Int](transactions, 0.1, 2)
    val frequentItemSets = fpg.frequentItemsSets.collect()
    assert(frequentItemSets.length == 21)

    spark.stop()
  }
}
