package com.hilabs.analytics.fpg

import java.nio.{ByteBuffer, ByteOrder}
import java.security.MessageDigest
import com.hilabs.analytics.fpg.Query._
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Dataset, SparkSession}
import com.hilabs.analytics.fpg.QueryBasedRules._
import scala.collection.JavaConverters._

case class ByteHash(bytes: Array[Byte]) extends Serializable {
  override val hashCode = bytes.deep.hashCode
  override def equals(obj:Any) =
    obj.isInstanceOf[ByteHash] && obj.asInstanceOf[ByteHash].bytes.deep == this.bytes.deep
}

case class RuleAntecedentHash(consequent:Int, antecedent: List[Int], ruleFrequency: Long, antecedentHash: ByteHash)

class QueryBasedRules(spark: SparkSession) extends Serializable {

  import spark.implicits._

  private def patternsToRules(patterns: Dataset[(List[Int], Long)],
                              consequentItemQuery: ItemQuery[Int]): Dataset[RuleAntecedentHash] = {
    patterns.flatMap(p => {
      p._1.zipWithIndex.filter(r => consequentItemQuery(r._1)).map(r => {
        val antecedent = p._1.slice(0, r._2) ++ p._1.slice(r._2+1, p._1.length)
        RuleAntecedentHash(r._1, antecedent, p._2, hash(antecedent))
      })
    })
  }

  def generateRules(freqRankPatterns: Dataset[(List[Int], Long)],
                    fnPatternCount:RDD[Array[Int]] => Dataset[(List[Int], Long)],
                    consequentItemQuery: ItemQuery[Int],
                    frequencyByRank: Map[Int, Long],
                    transactionCount: Long) = {
    val _rules =
      patternsToRules(freqRankPatterns, consequentItemQuery)
    val antecedentFrequency = fnPatternCount(_rules.map(r => r.antecedent.toArray).rdd)
    val antecedentFrequencyWithHash =
      antecedentFrequency
        .map(x => (x._1, x._2, hash(x._1)))
        .toDF("Antecedent", "AntecedentFrequency", "AntecedentHash")
        .as[(List[Int], Long, ByteHash)]

    antecedentFrequencyWithHash
      .join(_rules, antecedentFrequencyWithHash("AntecedentHash").equalTo(_rules("antecedentHash")), "inner")
      .map(r => {
        ARule(
          r.getInt(3),
          r.getList[Int](0).asScala.toList,
          r.getLong(5),
          r.getLong(1),
          frequencyByRank(r.getInt(3)),
          transactionCount)
      })
  }
}

object QueryBasedRules {
  def hash(pattern: List[Int]): ByteHash = {
    val buf = ByteBuffer.allocate(4 * pattern.size)
    buf.order(ByteOrder.BIG_ENDIAN)
    val barr =  pattern.foldLeft(buf)((buffer, item) => buffer.putInt(item)).array()
    ByteHash(MessageDigest.getInstance("MD5").digest(barr))
  }
}
