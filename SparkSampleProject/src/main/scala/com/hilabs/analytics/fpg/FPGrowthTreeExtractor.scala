package com.hilabs.analytics.fpg

import org.apache.log4j.Logger
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import scala.reflect.ClassTag

/**
 * A parallel FP-growth algorithm to mine frequent itemsets. The algorithm is described in
 * <a href="http://dx.doi.org/10.1145/1454008.1454027">Li et al., PFP: Parallel FP-Growth for Query
 * Recommendation</a>. PFP distributes computation in such a way that each worker executes an
 * independent group of mining tasks. The FP-Growth algorithm is described in
 * <a href="http://dx.doi.org/10.1145/335191.335372">Han et al., Mining frequent patterns without
 * candidate generation</a>.
 * There are many custom defined types in this file.
 * Please take a look at FPTypes.scala for any clarification on custom defined types.
 *
 * @see <a href="http://en.wikipedia.org/wiki/Association_rule_learning">
 * Association rule learning (Wikipedia)</a>
 *
 */
class FPGrowthTreeExtractor[Item: ClassTag] (val spark: SparkSession, fpTrees: Dataset[FPTreeItem[Item]]) extends Serializable with FPTypes[Item] {
  import spark.sqlContext.implicits._
  @transient lazy val logger: Logger = Logger.getLogger("com.hilabs.analytics.spark.ml.association_mining.com.hilabs.analytics.fpg")

  /**
   * Customizes minimum support value for each item so that items with larger frequencies can have a higher support value
   * @param itemFreq Array of items along with their frequencies
   * @param LS The default support value for all items
   * @param usePerItemSupport parameter to dictate whether or not to have customized support values
   * @return A map from item to the support value
   */
  def itemsMIPmapRank(itemFreq: Array[(Item, Frequency)], LS: Long, usePerItemSupport: Boolean): Map[Item, MISValue] = {
    var result = Map[Item, Long]().withDefaultValue(LS)
    if (usePerItemSupport) {
      result = result ++ {
        val itemProbability = itemFreq.map(i => (i._1, 1 / i._2.toDouble))
        val itemMISValue = itemProbability.map(i => (i._1, math.round(1 / (-i._2 * math.log(i._2)))))
        val itemsMIS: Array[(Item, MISValue)] = itemMISValue.map(i => if (i._2 >= LS) (i._1, i._2) else (i._1, LS))
        itemsMIS.toMap
      }
    }
    result
  }

  lazy val itemAndSupportByRank = selectColumns(fpTrees.toDF(), Seq("rank", "item", "frequency"))

  lazy val itemByRank = itemAndSupportByRank
    .foldLeft(Map[Rank, Item]())((m, i) => m + (i.getInt(0) -> i.getAs[Item](1)))

  lazy val rankByItem = itemByRank.map(kv => (kv._2, kv._1))

  lazy val frequencyByRank = itemAndSupportByRank
    .foldLeft(Map[Rank, Long]())((m, i) => m + (i.getInt(0) -> i.getLong(2)))

  /**
   * extracts all the patterns given all the trees
   * @param query
   * @return All the patterns given all the trees
   */
  def extractPatterns(query: Query[Item],
                      minPatternFrequency: Long,
                      maxLength: Int,
                      maxNodes: Long,
                      usePerItemSupport: Boolean = false): Dataset[(List[Rank], Frequency)] = {
    logger.debug(s"Extracting patterns with the following parameters: minPatternFrequency=$minPatternFrequency, maxLength=$maxLength , querySize=${query.patternQueries.size}, usePerItemSupport=$usePerItemSupport")

    val misVals =
      itemsMIPmapRank(
        itemAndSupportByRank.map(r => (r.getAs[Item](1), r.getLong(2))),
        minPatternFrequency,
        usePerItemSupport)

    val freqPatterns: Dataset[(List[Rank], Frequency)] =
      fpTrees
        .flatMap(record => {
          getRecordPatterns(
            record,
            query.mapQueries(itemByRank).cachePatternQueries,
            maxLength,
            maxNodes,
            misVals(itemByRank(record.rank)))
        })
    freqPatterns
  }

  def mapRankToItem(freqPatterns: Dataset[(List[Rank], Frequency)]): RDD[FreqItemset[Item]] = {
    freqPatterns
      .rdd
      .map {
        case (ranks, count) =>
          new FreqItemset(ranks.map(i => itemByRank(i)).toArray, count)
      }
  }

  private def toQueryDataset (rdd: RDD[(Rank, Array[Byte])]): Dataset[QueryTree] = {
    val queryTreeRdd: RDD[QueryTree] =
      rdd
        .map {
          case (rank, treeArr) => {
            QueryTree(rank, treeArr)
          }
        }
    spark.createDataset(queryTreeRdd)
  }

  def extract(itemFpTree: FPTreeItem[Item], queryTreeItem: QueryTree): Iterator[(List[Rank], Frequency)] = {
    val fpTree = FPTree.deserializeMulti(itemFpTree.serializedFPTree)
    val queryTree =  TransactionTree.deserializeMulti(queryTreeItem.serializedTree)
    fpTree.extract(queryTree).map {case (t, c) => (queryTreeItem.rank :: t, c)}
  }

  def processTransactionChunk(transactionChunk: Seq[Array[Rank]], chunkIndex: Int): Map[Rank, Array[Byte]] = {
    logger.debug(s"Processing pattern chunk $chunkIndex")
    logger.debug("transaction chunk size is " + transactionChunk.size)
    transactionChunk
      .map(transaction => (transaction.head, (transaction.tail, 1)))
      .groupBy(_._1)
      .map {
        case (part, transactions) =>
          (part, TransactionTree.serialize(
            transactions
              .map(_._2)
              .foldLeft(new TransactionTree())((tree, t) => tree.add(t._1))))
      }
  }

  def buildSerializedQueryTrees(queryTransactionsRdd: RDD[Array[Int]]) = {
    queryTransactionsRdd
      .filter(!_.isEmpty)
      .map(transaction =>
        transaction.sorted.reverse)
      .mapPartitions(it =>
        it.grouped(200000).zipWithIndex.flatMap((processTransactionChunk _).tupled))
      .aggregateByKey(Array[Byte]())(TransactionTree.aggregateSingle, TransactionTree.aggregateMulti)
  }

  def extractPatterns(queryTransactionsRdd: RDD[Array[Int]]): Dataset[(List[Int], Frequency)] = {
    val queryTreeDataset =
      toQueryDataset(buildSerializedQueryTrees(queryTransactionsRdd))
    fpTrees
      .joinWith(queryTreeDataset, fpTrees("rank") === queryTreeDataset("rank"), "inner")
      .flatMap {
        case (itemFpTree: FPTreeItem[Item], queryTree: QueryTree) =>
          extract(itemFpTree, queryTree)
      }
  }

  /**
   * Gets the patterns from a given FPTree
   * @param record A single FPTree
   * @param query
   * @return All the patterns from the given FPTree
   */
  private def getRecordPatterns(record: FPTreeItem[Item],
                                query: => Query[Int] = Query.IntEmptySubsetQuery,
                                maxLength: Int,
                                maxNodes: Long,
                                mis: MISValue): Iterator[(List[Rank], Frequency)] = {
    query.reduceIfMatches(record.rank) match {
      case Some(subQuery:Query[Int]) => {
        val rankPatternIter =
          if (query.matches(record.rank))
            Iterator.single((List(record.rank), record.rootCount))
          else
            Iterator.empty
        lazy val treeStats = s"item-${record.item.toString}, rank-${record.rank}, item count-${record.frequency}, " +
          s"big tree flag-${record.isBigTree}, minItemSupport-$mis, " +
          s"num trees-${record.numTrees}, root count-${record.rootCount}"

        if (record.rootCount >= mis) {
          rankPatternIter ++ {
            if (record.isBigTree) {
              logger.debug(s"Big Tree Extract: $treeStats")
              FPTree
                .bigTreeExtract(record.serializedFPTree, subQuery, maxLength - 1, maxNodes, mis)
                .map{case (t, c) => (record.rank :: t, c)}
            } else {
              val tree = FPTree.deserializeMulti(record.serializedFPTree)
              logger.debug(s"Regular Tree Extract: $treeStats, tree nd count-${tree.ndCnt}")
              tree
                .extract(subQuery, maxLength - 1, mis)
                .map{case (t, c) => (record.rank :: t, c)}
            }
          }
        } else {
          Iterator.empty
        }
      }
      case None => {
        Iterator.empty
      }
    }
  }


  /**
   * Selects columns from a dataset of FpTreeItems
   * @param fpTrees A dataset of FpTreeItems
   * @param columns The columns to select from the dataset
   * @return The rows that were selected
   */
  private def selectColumns(fpTrees: DataFrame, columns: Seq[String]): Array[Row] = {
    val colNames = columns.map(name => col(name))
    fpTrees.select(colNames:_*).collect()
  }
}

case class QueryTree(rank: Int, serializedTree: Array[Byte])