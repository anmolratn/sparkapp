package com.hilabs.analytics.fpg

import scala.collection.mutable
import scala.util.matching.Regex
import com.hilabs.analytics.fpg.Query._

case class PatternQuery[T](exactMatch: Boolean, itemQueries: ItemQuery[T]*)
{
  def hash: Int =
    31 * exactMatch.hashCode() + itemQueries.foldLeft(1)((c, itemQuery) => c * itemQuery.hashCode())

  def matches(items: Seq[T], ignoreExactMatch: Boolean = false): Boolean  = {
    if (items.size < itemQueries.size || (!ignoreExactMatch && (exactMatch && items.size != itemQueries.size))) {
      false
    } else {
      itemQueries.forall(itemQuery => items.exists(itemQuery(_)))
    }
  }

  def removeFirstItemQueryMatching(item: T): Option[PatternQuery[T]] = {
    dropFirstMatch(itemQueries, item).map(PatternQuery(exactMatch, _:_*))
  }

  def mapItemQueries[Q](mapping: Map[Q, T]): PatternQuery[Q] = {
    PatternQuery(exactMatch, itemQueries.map(itemQuery => itemQuery.mapQuery(mapping)):_*)
  }

  def cacheItemQueries: PatternQuery[T] = {
    PatternQuery(exactMatch, itemQueries.map(_.cacheQuery()):_*)
  }

  def numItemQueries: Int = itemQueries.size
}

class EmptyPatternExactQuery[T] extends PatternQuery[T](true)
class EmptyPatternSubsetQuery[T] extends PatternQuery[T](false)

case class Query[T](patternQueries: PatternQuery[T]*) {

  def hash: Int = patternQueries.foldLeft(1)((c, patternQuery) => c*patternQuery.hash)

  def apply(query1: Query[T], query2: Query[T]): Query[T] = {
    Query((query1.patternQueries ++ query2.patternQueries):_*)
  }

  val _patternQueries: Seq[PatternQuery[T]] =
    if (patternQueries.contains(new EmptyPatternSubsetQuery[T]))
      Seq(new EmptyPatternSubsetQuery[T])
    else
      patternQueries

  def matches(items: Seq[T], ignoreExactMatch: Boolean = false): Boolean =
    _patternQueries.exists(_.matches(items, ignoreExactMatch))

  def matches(item: T):Boolean =
    matchesCache.getOrElseUpdate((hash, item.hashCode()), matches(Seq(item), true))

  def mapQueries[Q](mapping: Map[Q, T]): Query[Q] =
    Query(_patternQueries.map(_.mapItemQueries(mapping)):_*)

  def cachePatternQueries: Query[T] = Query(_patternQueries.map(_.cacheItemQueries):_*)

  def reduceIfMatches(item: T): Option[Query[T]] = {
    val matchingQueries: Seq[PatternQuery[T]] =
      _patternQueries
        .map(patternQuery => {
          patternQuery.removeFirstItemQueryMatching(item) match {
            case Some(pquery) => Some(pquery)
            case None =>
              if (patternQuery.exactMatch)
                None
              else
                Some(patternQuery)
          }
        })
        .filter(_.isDefined)
        .map(_.get)
    if (matchingQueries.nonEmpty)
      Some(Query(matchingQueries.distinct:_*))
    else
      None
  }

  def containsEmptyExactMatch: Boolean = _patternQueries.contains(new EmptyPatternExactQuery[T])

  def containsEmptySubsetMatch: Boolean = _patternQueries.contains(new EmptyPatternSubsetQuery[T])

  def containsEmptyMatch: Boolean =
    _patternQueries.exists(pq =>
      Seq(new EmptyPatternExactQuery[T], new EmptyPatternSubsetQuery[T]).contains(pq))

}

class EmptySubsetQuery[T] extends Query[T](new EmptyPatternSubsetQuery[T])

object Query {

  val IntEmptySubsetQuery = new EmptySubsetQuery[Int]

  val StringEmptySubsetQuery = new EmptySubsetQuery[String]

  val IntEmptyQuery = Query[Int]()

  val matchesCache = mutable.Map[(Int, Int), Boolean]()

  def dropFirstMatch[A](seq: Seq[ItemQuery[A]], value: A): Option[Seq[ItemQuery[A]]] = {
    val index = seq.indexWhere(_(value))
    if (index < 0) {
      None
    } else if (index == 0) {
      Some(seq.tail)
    } else {
      val (a, b) = seq.splitAt(index)
      Some(a ++ b.tail)
    }
  }

  def not[T](itemQuery: ItemQuery[T]): ItemQuery[T] = (item: T) => !itemQuery(item)

  def or[T](itemQuery1: ItemQuery[T], itemQuery2: ItemQuery[T]): ItemQuery[T] = {
    item:T => itemQuery1(item) || itemQuery2(item)
  }

  def and[T](itemQuery1: ItemQuery[T], itemQuery2: ItemQuery[T]): ItemQuery[T] = {
    item:T => itemQuery1(item) && itemQuery2(item)
  }

  def in[T](collection: Seq[T]): ItemQuery[T] = {
    item:T => collection.contains(item)
  }

  def in[T](collection: scala.collection.Set[T]): ItemQuery[T] = {
    item:T => collection.contains(item)
  }

  def eql[T](value: T): ItemQuery[T] = {
    item: T => item == value
  }

  def str(s: String): ItemQuery[String] = (str: String) => s == str

  def strStartsWith(prefix: String): ItemQuery[String] = (str: String) => str.startsWith(prefix)

  def regexMatch(regexStr: String): ItemQuery[String] = {
    val regex = new Regex(regexStr)
    str:String => regex.findFirstIn(str).isDefined
  }

  implicit class ItemQuery[T](query: T => Boolean) extends Serializable {
    def apply(input: T) = query(input)

    def cacheQuery() : ItemQuery[T] = {
      val cache = mutable.Map[T, Boolean]()
      item: T => cache.getOrElseUpdate(item, query(item))
    }

    def mapQuery[Q](mapping: Map[Q, T]): ItemQuery[Q] = {
      item: Q => query(mapping(item))
    }
  }
}


