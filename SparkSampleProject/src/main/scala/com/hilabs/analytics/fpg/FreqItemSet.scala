package com.hilabs.analytics.fpg

import scala.collection.JavaConverters._

class FreqItemset[Item] (
                          val items: Array[Item],
                          val freq: Long) extends Serializable {

  /**
   * Returns items in a Java List.
   *
   */
  def javaItems: java.util.List[Item] = {
    items.toList.asJava
  }

  override def toString: String = {
    s"${items.mkString("{", ",", "}")}: $freq"
  }
}
