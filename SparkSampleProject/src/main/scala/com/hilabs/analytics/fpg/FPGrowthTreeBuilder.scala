package com.hilabs.analytics.fpg

import java.{util => ju}

import com.hilabs.analytics.fpg.LoggingUtils._
import org.apache.log4j.Logger
import org.apache.spark.HashPartitioner
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Dataset, Encoder, SparkSession}

import scala.reflect.ClassTag


/**
 * A parallel FP-growth algorithm to mine frequent itemsets. The algorithm is described in
 * <a href="http://dx.doi.org/10.1145/1454008.1454027">Li et al., PFP: Parallel FP-Growth for Query
 * Recommendation</a>. PFP distributes computation in such a way that each worker executes an
 * independent group of mining tasks. The FP-Growth algorithm is described in
 * <a href="http://dx.doi.org/10.1145/335191.335372">Han et al., Mining frequent patterns without
 * candidate generation</a>.
 * There are many custom defined types in this file.
 * Please take a look at FPTypes.scala for any clarification on custom defined types.
 *
 * @see <a href="http://en.wikipedia.org/wiki/Association_rule_learning">
 * Association rule learning (Wikipedia)</a>
 *
 */
class FPGrowthTreeBuilder[Item: ClassTag] (val spark: SparkSession, transactions: RDD[Array[Item]]) extends Serializable with FPTypes[Item] {
  import spark.implicits._
  @transient lazy val logger: Logger = Logger.getLogger("com.hilabs.analytics.spark.ml.association_mining.com.hilabs.analytics.fpg")

  /**
   * Given a set of transactions extracts the patterns from these transactions
   * @return
   */
  def createFPTrees(query: Query[Item] = new EmptySubsetQuery[Item],
                    minItemFrequency: Long,
                    maxNodes: Long)
                   (implicit encoder: Encoder[FPTreeItem[Item]]): Dataset[FPTreeItem[Item]] = {
    logger.debug(s"Building FP trees with the following parameters: minItemFrequency=$minItemFrequency, querySize=${query._patternQueries.size}")
    val freqItemsCount =
      genFreqItems(minItemFrequency)
    logger.logFreqItemsCountInfo(freqItemsCount)
    val itemToRank: Map[Item, Rank] = generateItemToPartitionMap(freqItemsCount)
    logger.logItemToRankInfo(itemToRank)
    val freqItemFPTrees =
      generateFreqItemsFPTrees(
        transactions.filter(item => query.matches(item, ignoreExactMatch = true)), itemToRank, maxNodes)
    toDataset(freqItemFPTrees, freqItemsCount)
  }

  /**
   * This method generates a mapping from each item to its partition, generating a unique partition for each distinct item
   * @param freqItemsCount An array of tuples containing items along with their frequency across all transactions
   * @return A Map from each item to its partition
   */
  private def generateItemToPartitionMap(freqItemsCount: Array[(Item, Frequency)]): Map[Item, Rank] = {
    val freqItems = freqItemsCount.map(_._1)
    freqItems.zipWithIndex.toMap
  }

  /**
   * Takes as input an rdd of tree definitions and converts it into a dataset with type FPTreeItem
   * @param rdd An rdd of FPTrees with a distinct tree for each rank
   * @param freqItemsCount An array of tuples of items along with their frequencies
   * @return A dataset of FPTreeItems
   */
  def toDataset(rdd: RDD[FPTreeDef], freqItemsCount: Array[(Item, Frequency)])
               (implicit encoder: Encoder[FPTreeItem[Item]]): Dataset[FPTreeItem[Item]] = {
    val rddFPTree: RDD[FPTreeItem[Item]] =
      rdd
        .map {
          case (rank, isBigTree, rootCount, treeArr, numBigTrees) => {
            val (item, frequency) = freqItemsCount(rank)
            FPTreeItem(item, frequency, rank, isBigTree, rootCount, treeArr, numBigTrees)}}

    rddFPTree.toDS()

  }

  /**
   * Generates the FPTrees given all the transactions
   * @param transactions the entire transaction data represented as Array of Items
   * @param itemToRank A map from each item to its partition where distinct items have their own partition
   * @return FPTrees derived from all the transactions
   */
  private def generateFreqItemsFPTrees(transactions: RDD[Transaction], itemToRank: Map[Item, Rank], maxNodes: Long):RDD[FPTreeDef] = {
    val rankSortedFilteredData: RDD[(RankedTransaction, Frequency)] =
      transactions
        .map(x =>
          ({
            val filtered = x.flatMap(itemToRank.get)
            ju.Arrays.sort(filtered)
            filtered
          }, 1L))
    val mapSideTreesRdd = mapSideRDD(rankSortedFilteredData)
    val aggregateTreesRdd = combineFPTrees(mapSideTreesRdd)
    reduceSideRDD(aggregateTreesRdd, maxNodes)
  }

  /**
   * Generates frequent items by filtering the input data using minimal support level.
   * @return array of frequent patterns and their frequencies ordered by their frequencies in decreasing order
   */
  private def genFreqItems(minItemFrequency: Long): Array[(Item, Frequency)] = {
    val partitioner = new HashPartitioner(transactions.partitions.length)
    transactions
      .flatMap(v => v.map((_, 1L)))
      .reduceByKey(partitioner, _ + _)
      .filter {case (_, count) => count >= minItemFrequency}
      .map {case (item, count) => (item, count)}
      .sortBy {case (item, count) => (-count, item.toString)}
      .collect()
  }

  /**
   * From a ranked transaction generates multiple conditional transactions
   *
   * @param rankedTransaction a ranked transaction
   * @param count number of times that this ranked transaction occurs over entire dataset
   * @return a seq of tuples where the first element is the rank of the item that the output transaction is conditioned on
   *         and the second element is a tuple with the first element being the conditional transaction and the second element
   *         being the input count parameter
   */
  private def genCondTransactions(rankedTransaction: RankedTransaction, count: Long): Seq[(Rank, (TransactionSlice, Frequency))] =
    for (i <- rankedTransaction.indices) yield (rankedTransaction(i), (rankedTransaction.slice(0, i), count))


  /**
   * Takes a chunk of ranked transactions and returns a map from rank to its associated serialized FPTree
   * @param transactionChunk A chunk of ranked transactions along with their frequencies
   * @return A Map from rank to its associated serialized FPTree
   */
  private def processTransactionChunk(transactionChunk: Seq[(RankedTransaction, Frequency)], chunkIndex: Int): Map[Rank, Array[Byte]] = {
    logger.debug(s"Processing pattern chunk $chunkIndex")
    transactionChunk
      .flatMap(transaction =>
        genCondTransactions(transaction._1, transaction._2))
      .groupBy(_._1)
      .map {
        case (part, transactions) =>
          (part, FPTree.serialize(
            transactions
              .map(_._2)
              .foldLeft(FPTree())((tree, t) => tree.add(t._1, t._2))))
      }
  }

  /**
   * Takes ranked transactions with their frequencies and returns an rdd of tuples containing serialized FPTrees along with their ranks
   * @param transactions all the transactions represented as an array of items
   * @return an rdd of tuples containing a serialized FPTree along with its rank for each chunk of ranked transactions
   */
  private def mapSideRDD(transactions: RDD[(RankedTransaction, Frequency)]):RDD[(Rank, SerializedFPTree)] = {
    transactions
      .mapPartitions(transactionsIter => {
        transactionsIter
          .grouped(20000)
          .zipWithIndex
          .flatMap((processTransactionChunk _).tupled)
      })
  }

  /**
   * Generate frequent itemsets by building FP-Trees, the extraction is done on each partition.
   * @param aggregateTreesSerialized An rdd of tuples with serialized FPTree and Rank
   * @return an rdd containing the entire definition of a tree as (rank, isBigTree, serializedtree, numTrees)
   */
  private def reduceSideRDD(aggregateTreesSerialized: RDD[(Rank, SerializedFPTree)], maxNodes: Long): RDD[FPTreeDef] = {
    aggregateTreesSerialized
      .map { case (rank, aggregatedMultiTree: Array[Byte]) =>
        var tree = FPTree()
        var isBigTree = false
        var multiTreeArr = Array[Byte]()
        var numBigTreeArrs = 0
        var rootCount = 0L
        val serializedTreeIter = FPTree.serializedMultiTreeIter(aggregatedMultiTree)

        while(serializedTreeIter.hasNext) {
          tree.merge(FPTree.deserialize(serializedTreeIter.next()))
          if (tree.ndCnt > maxNodes) {
            if (!isBigTree) {
              logger.info(s"FPTree for item with rank $rank is too large")
              isBigTree = true
            }
            logger.debug(s"big tree rank: $rank, node count: ${tree.ndCnt}, root count: ${tree.root.count}")
            multiTreeArr =  FPTree.aggregateSingle(multiTreeArr, tree)
            numBigTreeArrs += 1
            rootCount += tree.root.count
            tree = FPTree()
          }
        }

        if (isBigTree) {
          multiTreeArr =  FPTree.aggregateSingle(multiTreeArr, tree)
          numBigTreeArrs += 1
          rootCount += tree.root.count
          (rank, isBigTree, rootCount, multiTreeArr, numBigTreeArrs)
        } else {
          logger.debug(s"regular tree rank: $rank, node count: ${tree.ndCnt}, root count: ${tree.root.count}")
          (rank, isBigTree, tree.root.count, FPTree.aggregateSingle(Array[Byte](), tree), 0)
        }
      }
  }

  /**
   * Combines all the serialized FPTrees with the same rank
   * @param mapSideRDD an rdd with all the serialized FPTree and rank tuples for the ranked transaction chunks
   * @return an rdd that captures the result of combining all the serialized FPTrees with the same rank
   */
  private def combineFPTrees(mapSideRDD: RDD[(Rank, SerializedFPTree)]):RDD[(Rank, SerializedFPTree)] = {
    mapSideRDD
      .aggregateByKey(Array[Byte]())(FPTree.aggregateSingle, FPTree.aggregateMulti)
  }
}
/**
 * A case class to represent a FPTree
 * @param item The item for which this conditional FPTree has been built
 * @param rank The Rank of this item
 * @param isBigTree A boolean to specify whether or not this is a big tree
 * @param serializedFPTree Serialized version of the tree
 * @param numTrees The number of individual trees that make up this tree(1 if this tree is not a big tree)
 * @tparam Item
 */
case class FPTreeItem[Item](item: Item, frequency: Long, rank: Int, isBigTree: Boolean, rootCount: Long, serializedFPTree: Array[Byte], numTrees: Int)