package com.hilabs.analytics.fpg

import java.nio.ByteBuffer

import scala.annotation.tailrec


trait TreeSerializer[T] {
  def serialize(obj: T): Array[Byte]
  def deserialize(bytes: Array[Byte]): T
  def merge(t1: T, t2:T): T
  def emptyTree: T

  @tailrec
  private def deserializeMultiArr(multiArr: Array[Byte], accumulator: Seq[Array[Byte]] = Seq()): Seq[Array[Byte]] = {
    if (multiArr.isEmpty) {
      accumulator
    } else {
      val (subArrSizeSer, rest) = multiArr.splitAt(4)
      val subArrSize = ByteBuffer.wrap(subArrSizeSer).getInt()
      val (subArray, tail) = rest.splitAt(subArrSize)
      deserializeMultiArr(tail, accumulator :+ subArray)
    }
  }

  def serializedMultiTreeIter(multiArr: Array[Byte]): Iterator[Array[Byte]] = {
    deserializeMultiArr(multiArr)
      .filterNot(_.isEmpty)
      .toIterator
  }

  def deserializeMulti(multiTreeByteArr: Array[Byte]): T = {
    serializedMultiTreeIter(multiTreeByteArr)
      .foldLeft(emptyTree)((combined, part) => merge(combined, deserialize(part)))
  }

  def aggregateSingle(multiTreeByteArr: Array[Byte], part: Array[Byte]) : Array[Byte] = {
    multiTreeByteArr ++ ByteBuffer.allocate(4).putInt(part.length).array() ++ part
  }

  def aggregateSingle(multiTreeByteArr: Array[Byte], tree: T) : Array[Byte] = {
    aggregateSingle(multiTreeByteArr, serialize(tree))
  }

  def aggregateMulti(multiTreeByteArr1: Array[Byte], multiTreeByteArr2: Array[Byte]): Array[Byte] = {
    multiTreeByteArr1 ++ multiTreeByteArr2
  }
}
