package com.hilabs.analytics.fpg

import java.io.{ByteArrayOutputStream, DataOutputStream}
import java.nio.ByteBuffer
import com.hilabs.analytics.fpg.TransactionTree._
import org.xerial.snappy.Snappy

import scala.collection.mutable

class TransactionTree(val root:Node = Node(null), var ndCnt: Long = 0L) extends Serializable {

  def add(t: Iterable[Int], cons: Int = -1) = {
    var curr = root
    t.foreach { item =>
      curr = curr.children.getOrElseUpdate(item, {ndCnt += 1; Node(curr, item)})
    }
    curr.isLastItem = true
    if (cons >= 0) {
      curr.lastItemCons += cons
    }
    this
  }

  def transactions = {
    if (root.isLastItem)
      Iterator.single(List[Int]())
    else Iterator.empty
  } ++ root.transactions

  def merge(anotherTree: TransactionTree ): TransactionTree = {anotherTree.transactions.foreach(add(_)); this}
}

object TransactionTree extends TreeSerializer[TransactionTree] {

  case class Node(parent: Node, item: Int = -1) extends Serializable {
    val children: mutable.Map[Int, Node] = mutable.Map.empty
    var isLastItem: Boolean = false
    def isRoot: Boolean = parent == null
    def isLeaf: Boolean = children.isEmpty
    var lastItemCons: mutable.Set[Int] = mutable.Set.empty[Int]

    def transactions: Iterator[List[Int]] = {
      children.flatMap {
        case (item, node) =>
          val it =
            if (node.isLastItem) {
              Iterator.single(List(item))
            } else
              Iterator.empty
          it ++ node.transactions.map(t => item :: t)
      }.toIterator
    }
  }

  override def emptyTree = new TransactionTree()

  override def serialize(tree: TransactionTree) = {
    val baos = new ByteArrayOutputStream()
    val dos = new DataOutputStream(baos)
    dos.writeLong(tree.ndCnt)
    val nodeStck = mutable.Stack[Node]()
    nodeStck.push(tree.root)
    while (!nodeStck.isEmpty) {
      val nd = nodeStck.pop()
      dos.writeInt(nd.item)
      dos.writeInt(nd.children.size)
      dos.writeBoolean(nd.isLastItem)
      nodeStck.pushAll(nd.children.values)
    }

    dos.flush()
    dos.close()
    Snappy.compress(baos.toByteArray)
  }

  def isBitSet(byte: Byte): Boolean =
    ((byte >> 0) & 1) == 1

  override def deserialize(bytes: Array[Byte]): TransactionTree = {
    val (ndCnt, rootNd) = deserializeGetRootNodeAndCount(bytes)
    new TransactionTree(rootNd, ndCnt)
  }

  def deserializeGetRootNodeAndCount(bytes: Array[Byte]) = {
    val wrapped = ByteBuffer.wrap(Snappy.uncompress(bytes))
    val ndCnt = wrapped.getLong()
    def deserializeNode(parent: Node): (Int, Node) = {
      val item = wrapped.getInt()
      val nd: Node = Node(parent, item)
      val numChildren = wrapped.getInt()
      nd.isLastItem =  isBitSet(wrapped.get())

      (1 to numChildren).map(_ => {
        deserializeNode(nd) match {
          case (item, child) => nd.children += (item -> child)}})
      (nd.item, nd)
    }

    (ndCnt, deserializeNode(null)._2)
  }

  override def merge(t1: TransactionTree, t2: TransactionTree): TransactionTree = t1.merge(t2)
}
