package com.hilabs.analytics.fpg

import org.apache.log4j.Logger

import scala.reflect.ClassTag

object LoggingUtils {
  implicit class LoggingOps[Item: ClassTag](logger: Logger) extends FPTypes[Item] {
    def logFreqItemsCountInfo(freqItemsCount: Array[(Item, Long)]): Unit = {
      logger.info(s"No. of frequent items=${freqItemsCount.length}")
      logger.debug("Frequent item counts = \n" + freqItemsCount.map(item => s"${item._1.toString} - ${item._2}").mkString("\n"))
    }

    def logItemToRankInfo(itemToRank: Map[Item, Rank]): Unit = {
      if (logger.isDebugEnabled) {
        itemToRank
          .groupBy(_._2)
          .foreach {
            case (partition, itemRanks) =>
              logger.debug(s"Partition Item Mapping $partition: ${itemRanks.map(_._1.toString).mkString(",")}")
          }
        for (item <- itemToRank.toSeq.sortBy(_._2)) {
          logger.debug("item: " + item._1 +" rank: " + item._2)
        }
      }
    }
  }
}
