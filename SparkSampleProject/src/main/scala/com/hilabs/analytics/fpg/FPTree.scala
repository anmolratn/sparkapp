package com.hilabs.analytics.fpg

import java.io.{ByteArrayOutputStream, DataOutputStream}
import java.nio.ByteBuffer
import java.util.UUID
import com.hilabs.analytics.fpg.TransactionTree.{Node => QueryTreeNode}
import org.apache.log4j.Logger
import org.apache.spark.sql.SparkSession
import org.xerial.snappy.Snappy
import com.hilabs.analytics.fpg.FPTree._
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.util.Try


/**
 * FP-Tree data structure used in FP-Growth.
 */
class FPTree(val root: Node = new Node(null),
             val summaries : mutable.Map[Int, Summary] = mutable.Map.empty,
             var ndCnt : Long = 0) extends Serializable {

  lazy val id: String = UUID.randomUUID().toString
  @transient lazy val logger = Logger.getLogger("com.hilabs.analytics.spark.ml.association_mining.com.hilabs.analytics.fpg")

  /** Adds a transaction with count. */
  def add(t: Iterable[Int], count: Long = 1L): this.type = {
    require(count > 0)
    var curr = root
    curr.count += count
    t.foreach { item =>
      val summary = summaries.getOrElseUpdate(item, new Summary)
      summary.count += count
      val child = curr.children.getOrElseUpdate(item, {
        val newNode = new Node(curr)
        newNode.item = item
        summary.nodes += newNode
        ndCnt += 1
        newNode
      })
      child.count += count
      curr = child
    }
    this
  }

  /** Merges another FP-Tree. */
  def merge(other: FPTree): this.type = {
    other.transactions.foreach { case (t, c) =>
      add(t, c)
    }
    this
  }

  /** Gets a subtree with the suffix. */
  private def project(suffix: Int): FPTree = {
    val tree = new FPTree
    if (summaries.contains(suffix)) {
      val summary = summaries(suffix)
      summary.nodes.foreach { node =>
        var t = List.empty[Int]
        var curr = node.parent
        while (!curr.isRoot) {
          t = curr.item :: t
          curr = curr.parent
        }
        tree.add(t, node.count)
      }
    }
    tree
  }

  /** Returns all transactions in an iterator. */
  def transactions: Iterator[(List[Int], Long)] = getTransactions(root)

  /** Returns all transactions under this node. */
  private def getTransactions(node: Node): Iterator[(List[Int], Long)] = {
    var count = node.count
    node.children.iterator.flatMap { case (item, child) =>
      getTransactions(child).map { case (t, c) =>
        count -= c
        (item :: t, c)
      }
    } ++ {
      if (count > 0) {
        Iterator.single((Nil, count))
      } else {
        Iterator.empty
      }
    }
  }

  def removeLowCountItems(minCount: Long) = {
    val lowCountItems = summaries.map(s => (s._1, s._2.count)).filter(_._2 < minCount).keys
    lowCountItems.foreach(item => {
      val summary = summaries.remove(item).get
      summary.nodes.foreach(nd =>
      {
        nd.children.values.foreach(childNode => childNode.parent = nd.parent)
        nd.parent.children.remove(nd.item)
        nd.parent.children ++= nd.children
      })
      ndCnt -= summary.nodes.size
    })
    this
  }

  def cntNodes(node: FPTree.Node = root) = {
    def _cntNodes(nd: Node = root): Int = {
      1 + nd.children.map(x => _cntNodes(x._2)).sum
    }
    _cntNodes(node)
  }

  def extract(node: QueryTreeNode): Iterator[(List[Int], Long)] = {
      node.children.toIterator.flatMap {
        case (rank, childNode) => {
          if (summaries.contains(rank)) {
            val childExtract = project(rank).extract(childNode)
            if (childNode.isLastItem) {
              Iterator.single((List(rank), summaries(rank).count)) ++ childExtract.map { case (t, c) => (rank :: t, c)}
            } else {
              childExtract.map { case (t, c) => (rank :: t, c)}
            }
          } else
            Iterator.empty
        }
      }
  }

  def extract(queryTree: TransactionTree): Iterator[(List[Int], Long)] = {
    if (queryTree.root.isLastItem)
      Iterator.single((List[Int](), root.count))
    else
      Iterator.empty
  } ++ extract(queryTree.root)

  def extract(query: Query[Int],
              maxLength: Long,
              minItemSupport: Long): Iterator[(List[Int], Long)] = {
    if (maxLength <= 0 || query == Query.IntEmptyQuery){
      Iterator.empty
    } else {
      summaries
        .filter {case (_, summary) => summary.count >= minItemSupport}
        .flatMap {
          case (item, summary) =>
            query.reduceIfMatches(item) match {
              case Some(itemQuery) => {
                val itemExtract = project(item).extract(itemQuery, maxLength - 1, minItemSupport)
                if (itemQuery == Query.IntEmptySubsetQuery || query.matches(item))
                  Iterator.single((List(item), summary.count)) ++ itemExtract.map {case (t, c) => (item :: t, c)}
                else
                  itemExtract.map {case (t, c) => (item :: t, c)}
              }
              case None => Iterator.empty
            }
        }
        .toIterator
    }
  }
}

object FPTree extends TreeSerializer[FPTree]{

  @transient lazy val logger = Logger.getLogger("com.hilabs.analytics.spark.ml.association_mining.com.hilabs.analytics.fpg")

  def apply(root: Node = new Node(null),
            summaries : mutable.Map[Int, Summary] = mutable.Map.empty,
            ndCnt : Long = 0) = new FPTree(root, summaries, ndCnt)

  /** Representing a node in an FP-Tree. */
  class Node(var parent: Node) extends Serializable {
    var item: Int = -1
    var count: Long = 0L
    val children: mutable.Map[Int, Node] = mutable.Map.empty

    def isRoot: Boolean = parent == null
  }

  /** Summary of an item in an FP-Tree. */
  class Summary extends Serializable {
    var count: Long = 0L
    val nodes: ListBuffer[Node] = ListBuffer.empty
  }

  def maxNodes(spark: SparkSession, maxLength: Int): Long = {
    val result =
      Try {
        val conf = spark.sparkContext.getConf
        val executorMem = conf.getSizeAsBytes("spark.executor.memory")
        val numCores = conf.getInt("spark.executor.cores", 3)
        executorMem / (numCores  * 200 * maxLength)
      }.getOrElse(6000000L)
    logger.info(s"Max Nodes:$result")
    result
  }

  override def emptyTree = FPTree()

  override def serialize(tree: FPTree) = {
    val baos = new ByteArrayOutputStream()
    val dos = new DataOutputStream(baos)
    val nodeStck = mutable.Stack[Node]()
    nodeStck.push(tree.root)
    while (!nodeStck.isEmpty) {
      val nd = nodeStck.pop()
      dos.writeInt(nd.item)
      dos.writeLong(nd.count)
      dos.writeInt(nd.children.size)
      nodeStck.pushAll(nd.children.values)
    }

    dos.flush()
    dos.close()
    ByteBuffer.allocate(8).putLong(tree.ndCnt).array() ++ Snappy.compress(baos.toByteArray)
  }

  override def deserialize(bytes: Array[Byte]) = {
    val chunks = bytes.splitAt(8)
    val ndCnt = ByteBuffer.wrap(chunks._1).getLong()
    val wrapped = ByteBuffer.wrap(Snappy.uncompress(chunks._2))
    val summaries = mutable.Map.empty[Int, Summary]
    def deserializeNode(parent: Node): (Int, Node) = {
      val nd: Node = new Node(parent)
      nd.item = wrapped.getInt()
      nd.count = wrapped.getLong()
      val summary = summaries.getOrElseUpdate(nd.item, new Summary)
      summary.count += nd.count
      summary.nodes += nd
      val numChildren = wrapped.getInt()

      (1 to numChildren).map(_ => {
        deserializeNode(nd) match {
          case (item, child) => nd.children += (item -> child)}})
      (nd.item, nd)
    }
    val rootNd = deserializeNode(null)._2
    summaries.remove(-1)
    FPTree(rootNd, summaries, ndCnt)
  }

  override def merge(t1: FPTree, t2: FPTree): FPTree = t1.merge(t2)

  def bigTreeExtract(multiTreeSerialized: Array[Byte],
                     query: Query[Int],
                     maxLength: Int,
                     maxNodes: Long,
                     minPatternSupport: Long): Iterator[(List[Int], Long)] = {

    def projectSerialize(bytes: Array[Byte], query: Query[Int]) = {
      val serializedProjectedTrees = mutable.Map[Int, Array[Byte]]()
      val countByItem = mutable.Map[Int, Long]()
      val fnAddToSerializedTrees = (item: Int, tree: FPTree) => {
        val bytes = serializedProjectedTrees.getOrElseUpdate(item, Array[Byte]())
        countByItem.update(item, countByItem.getOrElse(item, 0L) + tree.root.count)
        serializedProjectedTrees.update(item, aggregateSingle(bytes, tree))
      }

      val it = serializedMultiTreeIter(bytes)
      val queryByItem = mutable.Map[Int, Query[Int]]()
      val nonMatching = mutable.Set[Int]()
      while(it.hasNext) {
        val tree = deserialize(it.next())
        tree
          .summaries
          .foreach {
            case (item, _) =>
              if(queryByItem.contains(item)) {
                fnAddToSerializedTrees(item, tree.project(item))
              }
              else if (!nonMatching.contains(item))
              {
                query.reduceIfMatches(item) match {
                  case Some(multiPatternQuery) =>
                    queryByItem.put(item, multiPatternQuery)
                    fnAddToSerializedTrees(item, tree.project(item))
                  case None => nonMatching.add(item)
                }
              }
          }
      }
      serializedProjectedTrees
        .keys
        .filter(k => countByItem(k) >= minPatternSupport)
        .map(k => (k, serializedProjectedTrees(k), queryByItem(k), countByItem(k)))
    }


    def extract(multiTreeSerialized: Array[Byte], itemQuery: Query[Int], maxLength: Int): Iterator[(List[Int], Long)] = {
      var tree = FPTree()
      var isBigTree = false
      var multiTreeArr = Array[Byte]()
      var numBigTreeArrs = 0
      val serializedTreeIter = FPTree.serializedMultiTreeIter(multiTreeSerialized)

      while(serializedTreeIter.hasNext) {
        tree.merge(FPTree.deserialize(serializedTreeIter.next()))
        if (tree.ndCnt > maxNodes) {
          if (!isBigTree) {
            isBigTree = true
          }
          multiTreeArr =  FPTree.aggregateSingle(multiTreeArr, tree)
          numBigTreeArrs += 1
          tree = FPTree()
        }
      }

      if (isBigTree) {
        _bigTreeExtract(multiTreeSerialized, itemQuery, maxLength)
      } else {
        tree.extract(itemQuery, maxLength, minPatternSupport)
      }
    }


    def _bigTreeExtract(byteArr: Array[Byte],
                        query: Query[Int],
                        maxLength: Int):Iterator[(List[Int], Long)] = {
      if (maxLength <= 0 || query == Query.IntEmptyQuery) {
        Iterator.empty
      } else {
        projectSerialize(byteArr, query)
          .flatMap {
            case (item, serializedTree, itemQuery, count) =>
                val itemExtract = extract(serializedTree, itemQuery, maxLength - 1)
                if (itemQuery == Query.IntEmptySubsetQuery || query.matches(item))
                  Iterator.single((List(item), count)) ++ itemExtract.map {case (t, c) => (item :: t, c)}
                else
                  itemExtract.map {case (t, c) => (item :: t, c)}
          }
          .toIterator
      }
    }

    _bigTreeExtract(multiTreeSerialized, query, maxLength)
  }
}
