package com.hilabs.analytics.fpg

import org.apache.spark.sql.{Dataset, Encoder, SparkSession}
import com.hilabs.analytics.fpg.Query._
import org.apache.log4j.Logger
import org.apache.spark.rdd.RDD
import scala.reflect.ClassTag


class ARules[Item: ClassTag](spark: SparkSession,
                             antecedentQuery: Query[Item],
                             consequentItemQuery: ItemQuery[Item],
                             maxRuleLength: Int, //rule length = antecedent length + consequent length(=1)
                             minItemSupport: Long,
                             minRuleSupport: Long)
                            (implicit encoder: Encoder[FPTreeItem[Item]],
                             encoder1: Encoder[GenericARule[Item]]) extends  Serializable {
  require(
    antecedentQuery.patternQueries.map(_.itemQueries.size).forall(_ + 1 <= maxRuleLength),
    "The sum of length of antecedent query and length of consequent query(=1) should be less than or equal to maximum rule length")

  import spark.implicits._

  @transient lazy val logger = Logger.getLogger("com.hilabs.analytics.fpg.AssociationRules")
  @transient lazy val maxNodes = FPTree.maxNodes(spark, maxRuleLength)


  val extractPatternQuery = {
    val patternQueries: Seq[PatternQuery[Item]] =
      antecedentQuery.patternQueries.map(
        patternQuery => PatternQuery[Item](patternQuery.exactMatch,
          (patternQuery.itemQueries :+ consequentItemQuery):_*))
    Query(patternQueries:_*)
  }

  def buildFPTrees(transactions: RDD[Array[Item]]): Dataset[FPTreeItem[Item]] = {
    val treeBuilder = new FPGrowthTreeBuilder[Item](spark, transactions)
    treeBuilder.createFPTrees(antecedentQuery, minItemSupport, maxNodes)
  }

  def extractFreqPatterns(treeExtractor: FPGrowthTreeExtractor[Item]): Dataset[(List[Int], Long)] = {
      val freqRankPatterns =
        treeExtractor.extractPatterns(extractPatternQuery, minRuleSupport, maxRuleLength, maxNodes)
      freqRankPatterns.as[(List[Int], Long)]
  }

  def treeExtractor(fpTrees: Dataset[FPTreeItem[Item]]) = new FPGrowthTreeExtractor[Item](spark, fpTrees)

  def generateRulesAndItemMapping(treeExtractor: FPGrowthTreeExtractor[Item],
                                  transactionCount: => Long) = {
    val freqPatterns = extractFreqPatterns(treeExtractor)
    val queryBasedRules = new QueryBasedRules(spark)
    val itemQuery = consequentItemQuery.mapQuery(treeExtractor.itemByRank).cacheQuery()
    val rules =
      queryBasedRules.generateRules(
        freqPatterns, treeExtractor.extractPatterns, itemQuery, treeExtractor.frequencyByRank, transactionCount)
    rules.as[ARule]
  }

  def generateRules(transactions: RDD[Array[Item]]) = {
    val fpTrees: Dataset[FPTreeItem[Item]] = buildFPTrees(transactions)
    val transactionCount: Long =
      transactions
        .filter(item => antecedentQuery.matches(item, ignoreExactMatch = true))
        .count
    val extractor = treeExtractor(fpTrees)
    generateRulesAndItemMapping(extractor, transactionCount)
      .map(rule =>
        GenericARule(
          extractor.itemByRank(rule.consequent),
          rule.antecedent.map(x => extractor.itemByRank(x)),
          rule.ruleFrequency,
          rule.antecedentFrequency,
          rule.consequentFrequency,
          rule.totalCount))
  }
}
