package com.hilabs.analytics.fpg

import org.apache.log4j.Logger
import org.apache.spark.sql.{Dataset, Encoder}
import com.hilabs.analytics.fpg.TransactionTree._
import scala.collection.mutable

case class GenericARule[Item](consequent: Item,
                              antecedent: List[Item],
                              ruleFrequency: Long,
                              antecedentFrequency: Long,
                              consequentFrequency: Long,
                              totalCount: Long) {
  def confidence = ruleFrequency.toDouble/antecedentFrequency
  def lift = (ruleFrequency.toDouble * totalCount)/(antecedentFrequency * consequentFrequency)
}

case class ARule(consequent: Int,
                 antecedent: List[Int],
                 ruleFrequency: Long,
                 antecedentFrequency: Long,
                 consequentFrequency: Long,
                 totalCount: Long) {
  def confidence = ruleFrequency.toDouble/antecedentFrequency
  def lift = (ruleFrequency.toDouble * totalCount)/(antecedentFrequency * consequentFrequency)
}

case class RuleTree(override val root: Node) extends TransactionTree {

  val consequent: Int = root.item

  override def add(t: Iterable[Int], cons: Int = -1): RuleTree = {
    val transaction = t.filter(_ != consequent).toList.sorted
    super.add(transaction, cons).asInstanceOf[RuleTree]
  }

  def merge(anotherTree: RuleTree): RuleTree = {
    if (this == RuleTree.emptyTree) {
      anotherTree
    } else if (anotherTree == RuleTree.emptyTree) {
      this
    } else {
      super.merge(anotherTree).asInstanceOf[RuleTree]
    }
  }

  def findRulesMatching(transaction: IndexedSeq[Int], forMissing: Boolean = false, cons: Int = -1): Iterator[List[Int]] = {
    if(!forMissing){
      val sortedCodes = transaction.sortWith((a, b) => (a == consequent) || (b != consequent && a <= b))
      if (transaction.isEmpty || sortedCodes.head != consequent) {
        Iterator.empty
      } else {
        _findRulesMatching(sortedCodes, root, 1, List.empty, cons)
      }
    }else{
      val sortedCodes = IndexedSeq(consequent) ++ transaction.sortWith((a, b) => (a <= b))
      if (transaction.contains(consequent)) {
        Iterator.empty
      } else {
        _findRulesMatching(sortedCodes, root, 1, List.empty, cons)
      }
    }
  }

  private def _findRulesMatching(transaction: IndexedSeq[Int], nd: Node, transactionIndex: Int, rule: List[Int], cons:Int = -1): Iterator[List[Int]] = {
    val it =
      if ((cons < 0 && nd.isLastItem) || (cons >= 0 && nd.lastItemCons.contains(cons)))
        Iterator.single(rule)
      else
        Iterator[List[Int]]()
    it ++ {
      if (nd.isLeaf || transactionIndex == transaction.length) {
        Iterator.empty
      } else {
        (transactionIndex until transaction.length).flatMap(i => {
          nd.children.get(transaction(i)) match {
            case Some(childNode) => _findRulesMatching(transaction, childNode, i + 1, rule :+ transaction(i), cons)
            case None => Iterator.empty
          }
        })
      }
    }
  }

  def aRuleMatches(transaction: IndexedSeq[Int], forMissing: Boolean = false) = findRulesMatching(transaction, forMissing).hasNext

}

object RuleTree extends TreeSerializer[RuleTree] {

  val logger = Logger.getLogger("com.hilabs.analytics.spark.ml.association_mining.com.hilabs.analytics.fpg")

  override def emptyTree = RuleTree(Node(null, -1))

  override def serialize(tree: RuleTree) = {
    TransactionTree.serialize(tree)
  }

  override def deserialize(bytes: Array[Byte]) = {
    val (ndCnt, root) = TransactionTree.deserializeGetRootNodeAndCount(bytes)
    val ruleTree = new RuleTree(root)
    ruleTree.ndCnt = ndCnt
    ruleTree
  }

  override def merge(t1: RuleTree, t2: RuleTree): RuleTree = t1.merge(t2)

  private def serializeRules(consequent: Int)(rules: Seq[ARule]) = {
    RuleTree.serialize(
      rules.foldLeft(RuleTree(Node(null, consequent)))((tree, rule) => tree.add(rule.antecedent)))
  }

  private def serializeRuleTreesByConsequent(iter: Iterator[ARule]) = {
    iter
      .grouped(2000000)
      .zipWithIndex
      .flatMap {
        case (ruleChunk, index) =>
          ruleChunk
            .groupBy(_.consequent)
            .map(x => (x._1, serializeRules(x._1)(x._2)))
      }
  }

  def buildRuleTreesByConsequent(rules: Dataset[ARule], maxNodeCnt: Long = 50000)(implicit encoder: Encoder[(Int, Array[Byte])]) = {
    rules
      .mapPartitions(serializeRuleTreesByConsequent)
      .rdd
      .aggregateByKey(Array[Byte]())(
        (t, r) => RuleTree.aggregateSingle(t, r),
        (t1, t2) => RuleTree.aggregateMulti(t1, t2))
      .flatMap {
        case (rank, treeMultiSer) =>
          val serializedTreeIter = RuleTree.serializedMultiTreeIter(treeMultiSer)
          val serTreeLst: mutable.Set[Array[Byte]] = mutable.Set[Array[Byte]]()
          var tree = RuleTree.emptyTree
          while(serializedTreeIter.hasNext) {
            tree = RuleTree.merge(tree, RuleTree.deserialize(serializedTreeIter.next()))
            if (tree.ndCnt > maxNodeCnt) {
              serTreeLst.add(RuleTree.aggregateSingle(Array[Byte](), tree))
              tree = RuleTree.emptyTree
            }
          }
          if (tree.ndCnt > 0)
            serTreeLst.add(RuleTree.aggregateSingle(Array[Byte](), tree))
          if (logger.isDebugEnabled && serTreeLst.size > 1) logger.debug(s"Number of rule trees:${serTreeLst.size}")
          serTreeLst.map(bytes => (rank, bytes))
      }
  }
}

