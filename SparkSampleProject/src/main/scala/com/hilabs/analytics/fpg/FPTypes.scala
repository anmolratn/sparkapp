package com.hilabs.analytics.fpg

/**
 * A set of types to be used in the FPG algorithm
 * @tparam Item A generic type associated with the transaction items
 */
trait FPTypes[Item] {
  type Rank = Int
  type Frequency = Long
  type MISValue = Long
  type Transaction = Array[Item]
  type RankedTransaction = Array[Rank]
  type TransactionSlice = Array[Rank]
  type SerializedFPTree = Array[Byte]
  type FPTreeDef = (Rank, Boolean, Long, SerializedFPTree, Int)
}
